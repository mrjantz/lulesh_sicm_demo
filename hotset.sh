#!/bin/bash

. $SPACK_DIR/share/spack/setup-env.sh
spack load sicm-high@develop%gcc@7.2.0

UPPER_NODE="1"

# Get the amount of free memory in the upper tier right now
COLUMN_NUMBER=$(echo ${UPPER_NODE} + 2 | bc)
UPPER_SIZE="$(numastat -m | awk -v column_number=${COLUMN_NUMBER} \
              '/MemFree/ {printf "%d * 1024 * 1024\n", $column_number}' | bc)"

# Get the peak RSS of the default run
#PEAK_RSS_DEFAULT=$(grep 'Maximum resident' unguided_run.out | awk '{print $6}')
#PEAK_RSS_DEFAULT_BYTES=$(echo "${PEAK_RSS_DEFAULT} * 1024" | bc)
#UPPER_SIZE=$(echo "${PEAK_RSS_DEFAULT_BYTES} * 0.5" | bc)

PROFILE_FILE="./profile.txt"
GUIDANCE_FILE="./guidance.txt"

  # Generate the guidance file
cat "${PROFILE_FILE}" | \
  sicm_hotset --capacity=${UPPER_SIZE} --weight=profile_extent_size \
  --node=${UPPER_NODE} > ${GUIDANCE_FILE}

