#!/bin/bash
set -x

. $SPACK_DIR/share/spack/setup-env.sh
spack load sicm-high@develop%gcc@7.2.0

export SH_UPPER_NODE="1"
export SH_LOWER_NODE="3"

COMMAND_WRAPPER=$(echo \
  "env time -v numactl --preferred=${SH_UPPER_NODE} " \
  "numactl --cpunodebind=${SH_UPPER_NODE} " \
  "--membind=${SH_UPPER_NODE},${SH_LOWER_NODE}" \
)

export OMP_NUM_THREADS="48"
export SH_DEFAULT_NODE="${SH_UPPER_NODE}"

COMMAND="${COMMAND_WRAPPER} ./lulesh2.0 -s 220 -i 5 -r 11 -b 0 -c 64 -p"
eval "${COMMAND}" 2>&1 | tee unguided_run.out

