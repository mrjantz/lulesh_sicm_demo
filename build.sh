#!/bin/bash

echo "Compiling lulesh with sicm-high."

. $SPACK_DIR/share/spack/setup-env.sh
spack load pgmath%gcc@7.2.0
spack load flang-patched%gcc@7.2.0
spack load llvm@flang-20180921%gcc@7.2.0
spack load sicm-high%gcc@7.2.0

# Define the variables for the compiler wrappers
export LD_LINKER="clang++ -Wno-unused-command-line-argument -L$(spack location -i flang-patched@20180921)/lib -lflang -lflangrti -Wl,-rpath,$(spack location -i llvm@flang-20180921)/lib -Wl,-rpath,$(spack location -i flang-patched@20180921)/lib -Wl,-rpath,$(spack location -i pgmath)/lib"
export LD_COMPILER="clang++ -Wno-unused-command-line-argument -march=x86-64" # Compiles from .bc -> .o
export CXX_COMPILER="clang++  -Wno-unused-command-line-argument -march=x86-64"
export C_COMPILER="clang   -Wno-unused-command-line-argument -march=x86-64"
export LLVMLINK="llvm-link"
export LLVMOPT="opt"

# Make sure the Makefiles find our wrappers
export COMPILER_WRAPPER="compiler_wrapper.sh "
export LD_WRAPPER="ld_wrapper.sh "
export PREPROCESS_WRAPPER="clang -E -x c -P"
export AR_WRAPPER="ar_wrapper.sh"
export RANLIB_WRAPPER="ranlib_wrapper.sh"

# Amount of context to add to each allocation site
export SH_CONTEXT="3"

rm -f *.o *.bc *.args *.mod contexts.txt nclones.txt nsites.txt buCG.txt .sicm_ir.bc .sicm_ir_transformed.bc
make clean
make

