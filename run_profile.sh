#!/bin/bash

. $SPACK_DIR/share/spack/setup-env.sh
spack load sicm-high@develop%gcc@7.2.0

export SH_UPPER_NODE="1"
export SH_LOWER_NODE="3"

COMMAND_WRAPPER=$(echo \
  "sudo env time -v numactl --preferred=${SH_UPPER_NODE} " \
  "numactl --cpunodebind=${SH_UPPER_NODE} " \
  "--membind=${SH_UPPER_NODE},${SH_LOWER_NODE}" \
)

export OMP_NUM_THREADS="47"
export SH_MAX_THREADS=`expr ${OMP_NUM_THREADS} + 1`

export SH_ARENA_LAYOUT="SHARED_SITE_ARENAS"
export SH_DEFAULT_NODE="${SH_UPPER_NODE}"

export SH_PROFILE_ALL="1"
export SH_PROFILE_ALL_EVENTS="MEM_LOAD_UOPS_RETIRED:L3_MISS"
export SH_PROFILE_EXTENT_SIZE="1"
export SH_PROFILE_RATE_NSECONDS=$(echo "10 * 1000000" | bc)  # 10ms
export SH_MAX_SAMPLE_PAGES="512"
export SH_SAMPLE_FREQ="16"

export SH_PROFILE_OUTPUT_FILE="./profile.txt"

echo 1 | sudo tee /proc/sys/kernel/perf_event_paranoid

COMMAND="${COMMAND_WRAPPER} ./lulesh2.0 -s 220 -i 12 -r 11 -b 0 -c 64 -p"
eval "${COMMAND}" 2>&1 | tee profiled_run.out

